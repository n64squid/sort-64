#define SQUID_RED		0x88
#define SQUID_GREEN		0xab
#define SQUID_BLUE		0xf7
#define SQUID_COLOUR_16 0x8D7D8D7D
#define COLOUR_COMPARE	0xF801F801
#define COLOUR_INSERT	0x72837283
#define COLOUR_SWAP		0x03010301
#define COLOUR_READ		0x18DD18DD

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

#define SCREEN_PADDING_HIGH 20
#define MENU_PADDING_HIGH 20

#define SCREEN_PADDING_LOW 10
#define MENU_PADDING_LOW 10
#define X_PADDING 25

#define ARRAY_SIZE_MAX 256

#define DRAW_BG_NO_DETACH 0
#define DRAW_BG_DETACH 1

typedef enum {
	SETTING_RES_LOW,
	SETTING_RES_HIGH,
} setting_res_t;

extern setting_res_t resolution;

display_context_t draw_bg(uint8_t detach);
void switch_resolution (uint8_t res);
