/* ====================================================================================
	 ____        _           _
	/ ___| _ __ | | __ _ ___| |__    ___
	\___ \| '_ \| |/ _` / __| '_ \  / __|
	 ___) | |_) | | (_| \__ | | | || (__
	|____/| .__/|_|\__,_|___|_| |_(_\___|
	      |_|

	This is the code for the spash screen that appears when you load the game. There
	isn't much interesting happening here beside the images appearing and then fading
	away. Also, if you hold START button until the fading completes, you activate low-res
	mode which is better for CRT TVs.

	This is the only part of the ROM which uses 32-bit colours, which are needed for
	the fading effect.

 ==================================================================================== */

#include <libdragon.h>

#define SPLASH_WAIT 300
#define SPLASH_FADE 5
#define SPLASH_BUFFERS 2

#define N64SQUID_LOGO_X 170
#define N64SQUID_LOGO_Y 80
#define MADE_WITH_LIBDRAGON_X 140
#define MADE_WITH_LIBDRAGON_Y 285

// Display the opening splash screen
void splash_screen(void) {

	// Initialise things
	display_context_t disp;
	display_init(RESOLUTION_640x480, DEPTH_32_BPP, SPLASH_BUFFERS, GAMMA_NONE, FILTERS_DISABLED);
	dfs_init(DFS_DEFAULT_LOCATION);
	int i = 0;

	// Load the N64 Squid logo
	int f = dfs_open("/n64squid_logo.sprite");
	sprite_t *n64squid_logo = malloc(dfs_size(f));
	dfs_read(n64squid_logo, 1, dfs_size(f), f);
	dfs_close(f);

	// Load the Libdragon logo
	f = dfs_open("/made_with_libdragon.sprite");
	sprite_t *made_with_libdragon = malloc(dfs_size(f));
	dfs_read(made_with_libdragon, 1, dfs_size(f), f);
	dfs_close(f);

	// Loop for a bunch of frames
	while (i < SPLASH_WAIT + 0x99) {
		// Wait for a free frame buffer
		while(!(disp = display_lock()));
		// For the first few frames, draw the logos
		if (i < SPLASH_FADE * SPLASH_BUFFERS) {
			graphics_draw_sprite(disp, N64SQUID_LOGO_X, N64SQUID_LOGO_Y, n64squid_logo);
			graphics_draw_sprite(disp, MADE_WITH_LIBDRAGON_X, MADE_WITH_LIBDRAGON_Y, made_with_libdragon);
		} else if (i < SPLASH_WAIT + SPLASH_FADE * SPLASH_BUFFERS) {
			//
		} else {
			// Then draw a box to make them fade away
			graphics_draw_box_trans(
				disp, N64SQUID_LOGO_X, N64SQUID_LOGO_Y, n64squid_logo->width, n64squid_logo->height,
				graphics_make_color (0, 0, 0, i - SPLASH_WAIT - SPLASH_FADE * SPLASH_BUFFERS)
				);
			graphics_draw_box_trans(
				disp, MADE_WITH_LIBDRAGON_X, MADE_WITH_LIBDRAGON_Y, made_with_libdragon->width, made_with_libdragon->height,
				graphics_make_color (0, 0, 0, i - SPLASH_WAIT - SPLASH_FADE * SPLASH_BUFFERS)
				);
		}

		// Complete the frame
		i += SPLASH_FADE;
		display_show(disp);
	}

	// Clean things up
	free(n64squid_logo);
	free(made_with_libdragon);
	display_close();
}
