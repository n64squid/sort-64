/* ====================================================================================
	 __  __
	|  \/  | ___ _ __  _   _   ___
	| |\/| |/ _ | '_ \| | | | / __|
	| |  | |  __| | | | |_| || (__
	|_|  |_|\___|_| |_|\__,_(_\___|

	This file contains the functions that deal with the menu where you can
	select all the various settings for Sort 64.

	It's pretty much just one big function that controls two screens:
	The algorithm select screen and the settings select screen.

 ==================================================================================== */

#include <libdragon.h>

#include "sort/algos/algo_table.h"
#include "common.h"
#include "menu.h"

#define TITLE_INDENT 140

#define ROW_WIDTH 180
#define ROW_HEIGHT_HIGH 16
#define ROW_HEIGHT_LOW 10
#define COL_LENGTH_HIGH 24
#define COL_LENGTH_LOW 16

#define SETTING_NAME_BUFFER 32
#define SETTING_MAX_OPTIONS 7
#define SETTING_COUNT 6

typedef struct {
	char title[SETTING_NAME_BUFFER];
	char options[SETTING_MAX_OPTIONS][SETTING_NAME_BUFFER];
	uint8_t len;
} setting_t;

// Default settings for the menu
uint8_t settings[SETTING_COUNT+1] = {
	0,
	SETTING_ARRAY_TYPE_RANDOM,
	SETTING_ARRAY_SIZE_256,
	SETTING_ARRAY_AUDIO_NONE,
	SETTING_ARRAY_IMAGE_NONE,
	SETTING_ARRAY_SPEED_3,
	SETTING_SKIP_INTRO,
};

void stage_menu(void) {
	// Initialise the display and controller variables
	display_context_t disp;
	struct controller_data keys;

	char algoname[64];
	int8_t selected = 0;

	// Select an algorithm
	algo_select:
	while (1) {
		uint8_t i, j;
		
		disp = draw_bg(DRAW_BG_DETACH);

		if (resolution == SETTING_RES_HIGH) {
			graphics_draw_text(
				disp,
				SCREEN_PADDING_HIGH + MENU_PADDING_HIGH + TITLE_INDENT,
				SCREEN_PADDING_HIGH + MENU_PADDING_HIGH,
				"Select the algorithm to visualise"
				);
		} else {
			graphics_draw_text(
				disp,
				SCREEN_PADDING_LOW + MENU_PADDING_LOW + TITLE_INDENT,
				SCREEN_PADDING_LOW + MENU_PADDING_LOW,
				"Select the algorithm to visualise"
				);
		}

		// Draw all the function names to the frame buffer
		for (i=0, j=0; i<ALGOTABLE_ROWS; i++, j++) {
			if (!list_of_algos.rows[i].vis) {
				j--;
				continue;
			}
			sprintf(algoname, "%c %s", selected==j ? '>' : ' ', list_of_algos.rows[i].name);
			if (resolution == SETTING_RES_HIGH) {
				graphics_draw_text(
					disp,
					SCREEN_PADDING_HIGH + MENU_PADDING_HIGH + X_PADDING + ROW_WIDTH*(j/COL_LENGTH_HIGH),
					SCREEN_PADDING_HIGH + MENU_PADDING_HIGH*5/2 + (ROW_HEIGHT_HIGH*(j%COL_LENGTH_HIGH)),
					algoname
					);
			} else {
				graphics_draw_text(
					disp,
					SCREEN_PADDING_LOW + MENU_PADDING_LOW + X_PADDING + ROW_WIDTH*(j/COL_LENGTH_LOW),
					SCREEN_PADDING_LOW + MENU_PADDING_LOW*5/2 + (ROW_HEIGHT_LOW*(j%COL_LENGTH_LOW)),
					algoname
					);
			}
		}

		// Send frame buffer to output
		display_show(disp);

		// Get controller input
		controller_scan();
		keys = get_keys_down();

		// Move the pointer around the screen
		if (keys.c[0].data) {
			if (keys.c[0].down) {
				selected = ((selected+1)%(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW)) + (selected/(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW))*(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			} else if (keys.c[0].up) {
				selected = ((selected-1)%(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW)) + (selected/(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW))*(resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			}else if (keys.c[0].right) {
				selected += (resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			} else if (keys.c[0].left) {
				selected -= (resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			} else if (keys.c[0].A) {
				// Go through the list of algos and skip the invisible ones to get the correct index
				for (i=0, j=0; i<ALGOTABLE_ROWS; i++, j++) {
					if (!list_of_algos.rows[i].vis) {
						j--;
						continue;
					}
					if (j == selected) {
						settings[SETTING_ALGORITHM] = i;
					}
				}
				break;
			}
			if (selected < 0) {
				selected += (resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			} else if (selected > j-1) {
				selected -= (resolution == SETTING_RES_HIGH?COL_LENGTH_HIGH:COL_LENGTH_LOW);
			}
		}
	}


	// Settings page
	selected = 0;
	while (1) {
		uint8_t i, j;
		// Menu settings
		setting_t settings_list[SETTING_COUNT] = {
			{"Array type", {"Random", "Reverse", "Repeating", "2xRandom", "Almost sorted"}},
			{"Array Size", {"256", "128", "64", "32", "16", "8"}},
			{"Sound", {"None", "Sine", "Square", "Triangle", "Sawtooth"}},
			{"Graphics", {"Bars", "Problem?", "Brewing", "Dragon"}},
			{"Speed", {"Zzz", "Slow", "Medium", "Fast", "Hol up", "FUUUUUU"}},
			{"Graphic skip level", {"None", "Intro", "Reads", "Swaps"}},
		};

		// Draw the background
		disp = draw_bg(DRAW_BG_DETACH);

		// Print the selected algorithm
		sprintf(algoname, "Selected algorithm: %s %li", list_of_algos.rows[settings[SETTING_ALGORITHM]].name, *(uint32_t*)settings);
		if (resolution == SETTING_RES_HIGH) {
			graphics_draw_text(
				disp,
				SCREEN_PADDING_HIGH + MENU_PADDING_HIGH+X_PADDING,
				SCREEN_PADDING_HIGH + MENU_PADDING_HIGH,
				algoname
				);
		} else {
			graphics_draw_text(
				disp,
				SCREEN_PADDING_LOW + MENU_PADDING_LOW + X_PADDING,
				SCREEN_PADDING_LOW + MENU_PADDING_LOW,
				algoname
				);
		}

		// Draw all the settings
		for (i=0; i<SETTING_COUNT; i++) {
			// Print category title
			if (resolution == SETTING_RES_HIGH) {
				graphics_draw_text(
					disp,
					SCREEN_PADDING_HIGH + MENU_PADDING_HIGH + X_PADDING,
					SCREEN_PADDING_HIGH + MENU_PADDING_HIGH*2 + ROW_HEIGHT_HIGH*i*3,
					settings_list[i].title
					);
			} else {
				graphics_draw_text(
					disp,
					SCREEN_PADDING_LOW + MENU_PADDING_LOW + X_PADDING,
					SCREEN_PADDING_LOW + MENU_PADDING_LOW*2 + ROW_HEIGHT_LOW*i*3,
					settings_list[i].title
					);
			}
			// Print each category option
			for (j=0; j<SETTING_MAX_OPTIONS; j++) {
				sprintf(algoname, "%c %s", settings[i+1]==j ? (selected==i?'>':'#') : ' ', settings_list[i].options[j]);
				if (resolution == SETTING_RES_HIGH) {
					graphics_draw_text(
						disp,
						SCREEN_PADDING_HIGH + MENU_PADDING_HIGH + X_PADDING + ROW_WIDTH*j/2,
						SCREEN_PADDING_HIGH + MENU_PADDING_HIGH*2 + ROW_HEIGHT_HIGH*(i*3+1),
						algoname
						);
				} else {
					graphics_draw_text(
						disp,
						SCREEN_PADDING_LOW + MENU_PADDING_LOW + X_PADDING + ROW_WIDTH*j/2,
						SCREEN_PADDING_LOW + MENU_PADDING_LOW*2 + ROW_HEIGHT_LOW*(i*3+1),
						algoname
						);
				}
			}
		}
		display_show(disp);

		// Get controller input
		controller_scan();
		keys = get_keys_down();
		if (keys.c[0].data) {
			if (keys.c[0].down) {
				selected = (selected+1) % SETTING_COUNT;
			} else if (keys.c[0].up) {
				selected = selected-1 % SETTING_COUNT;
			} else if (keys.c[0].right) {
				settings[selected+1]++;
				if (*settings_list[selected].options[settings[selected+1]] == 0) {
					settings[selected+1] = 0;
				}
			} else if (keys.c[0].left) {
				if (settings[selected+1] > 0) {
					settings[selected+1]--;
				}
			} else if (keys.c[0].B) {
				goto algo_select;
			} else if (keys.c[0].A) {
				return;
			}
			selected = selected < 0 ? SETTING_COUNT-1 : selected;
		}

	}
}
