/* ====================================================================================
	 ____            _      _____                      __   _  _
	| __ )  __ _ ___(_) ___|  ____   _ _ __   ___ ___ / /_ | || |   ___
	|  _ \ / _` / __| |/ __| |_ | | | | '_ \ / __/ __| '_ \| || |_ / __|
	| |_) | (_| \__ | | (__|  _|| |_| | | | | (__\__ | (_) |__   _| (__
	|____/ \__,_|___|_|\___|_|   \__,_|_| |_|\___|___/\___/   |_|(_\___|

	This is a copy of the basic_funcs.c from the sort repo and basic.c from the
	data-structures repo. The difference is that the version of the function in
	this file has display_vis() to update the N64 Libdragon visualiser for an
	amimated effect.

 ==================================================================================== */

#include <stdio.h>
#include <libdragon.h>

// Just include the one from the original sorter
#include "sort/algos/basic_funcs.h"
#include "visualiser.h"
#include "menu.h"
#include "common.h"

// Global vars to be only used in this file
static unsigned long long insertions;
static unsigned long long comparisons;
static unsigned long long swaps;
static unsigned long long reads;

metric_row metrics[BASIC_METRICS] = {
	{.name = "Comparisons",	.val = &comparisons},
	{.name = "Insertions",	.val = &insertions},
	{.name = "Swaps",		.val = &swaps},
	{.name = "Reads",		.val = &reads},
};

// Swap two elements
void swap(int* a, int* b) {
	swaps++;
	if (settings[SETTING_SKIP] < SETTING_SKIP_SWAPS) {
		display_vis(a, b, COLOUR_SWAP);
	}
	if (a && b && a!=b) {
		*a ^= *b;
		*b ^= *a;
		*a ^= *b;
	}
}

// Insert an item into the array
void insert(int* a, int b) {
	insertions++;
	display_vis(a, NULL, COLOUR_INSERT);
	if (a)
		*a = b;
}

// Compare two items, 'less than' version
char is_less_than(int a, int b) {
	comparisons++;
	display_vis(NULL, NULL, 0);
	return (a < b);
}
char is_less_than_p(int* a, int* b) {
	comparisons++;
	display_vis(a, b, COLOUR_COMPARE);
	return (*a < *b);
}

// Compare two items, 'greater than' version
char is_greater_than(int a, int b) {
	comparisons++;
	display_vis(NULL, NULL, 0);
	return (a > b);
}
char is_greater_than_p(int* a, int* b) {
	comparisons++;
	display_vis(a, b, COLOUR_COMPARE);
	return (*a > *b);
}

// Compare two items, 'equal to' version
char is_equal_to(int a, int b) {
	comparisons++;
	display_vis(NULL, NULL, 0);
	return (a == b);
}
char is_equal_to_p(int* a, int* b) {
	comparisons++;
	display_vis(a, b, COLOUR_COMPARE);
	return (*a == *b);
}

char is_greater_than_or_equal_to(int a, int b) {
	comparisons++;
	display_vis(NULL, NULL, 0);
	return (a >= b);
}
char is_greater_than_or_equal_to_p(int* a, int* b) {
	comparisons++;
	display_vis(a, b, COLOUR_COMPARE);
	return (*a >= *b);
}

char is_less_than_or_equal_to(int a, int b) {
	comparisons++;
	display_vis(NULL, NULL, 0);
	return (a <= b);
}
char is_less_than_or_equal_to_p(int* a, int* b) {
	comparisons++;
	display_vis(a, b, COLOUR_COMPARE);
	return (*a <= *b);
}

// Used when the actual value of a variable is read, not when it is being compared
int read_val(int a) {
	reads++;
	//display_vis(NULL, NULL, 0);
	return a;
}
int read_val_p(int* a) {
	reads++;
	display_vis(a, NULL, COLOUR_READ);
	return *a;
}

// Return and reset values
unsigned long long get_metric(unsigned char metric) {
	return *metrics[metric].val;
}

// Reset metrics
void reset_metrics(void) {
	for (int i=0; i<BASIC_METRICS; i++) {
		*metrics[i].val = 0;
	}
}
