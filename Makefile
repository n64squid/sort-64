V=1
GAME=sort64
SOURCE_DIR=src
BUILD_DIR=build
IMAGES_DIR=$(SOURCE_DIR)/images
IMAGE_FILES_ALL=$(wildcard $(IMAGES_DIR)/*.png)
IMAGE_FILES_LETTERS=$(foreach img,$(IMAGE_FILES_ALL),$(if $(findstring _,$(img)),$(img)))
IMAGE_FILES_NUMBERS=$(filter-out $(IMAGE_FILES_LETTERS), $(IMAGE_FILES_ALL))
SPRITE_FILES_NUMBERS=$(subst images,filesystem,$(IMAGE_FILES_NUMBERS:.png=.sprite))
SPRITE_FILES_LETTERS=$(subst images,filesystem,$(IMAGE_FILES_LETTERS:.png=.sprite))
AUDIO_DIR=$(SOURCE_DIR)/audio
AUDIO_FILES=$(wildcard $(AUDIO_DIR)/*.wav)
AUDIO64_FILES=$(subst audio,filesystem,$(AUDIO_FILES:.wav=.wav64))
FILESYSTEM_DIR=$(SOURCE_DIR)/filesystem
SRC=$(wildcard src/*.c)

TOOLS_DIR=libdragon/tools
TOOLS_AUDIOCONV=$(TOOLS_DIR)/audioconv64/audioconv64
TOOLS_MKSPRITE=$(TOOLS_DIR)/mksprite/mksprite

SORT_DIR=$(SOURCE_DIR)/sort
ALGO_DIR=$(SORT_DIR)/algos
DATA_SCTUCTS_DIR=$(SORT_DIR)/data-structures
ALGO_FILES=$(filter-out $(ALGO_DIR)/basic_funcs.c, $(wildcard $(ALGO_DIR)/*.c))
ALGO_OBJECTS=$(addprefix $(BUILD_DIR)/algos/,$(notdir $(subst .c,.o,$(ALGO_FILES))))
DATA_SCTUCTS_FILES=$(filter-out $(DATA_SCTUCTS_DIR)/basic.c, $(wildcard $(DATA_SCTUCTS_DIR)/*.c))
DATA_SCTUCTS_OBJECTS=$(addprefix $(BUILD_DIR)/structs/,$(notdir $(subst .c,.o,$(DATA_SCTUCTS_FILES))))

OBJS=$(addprefix $(BUILD_DIR)/,$(notdir $(subst .c,.o,$(SRC))))


all: $(GAME).z64
.PHONY: all

# Compile the required tools
$(TOOLS_AUDIOCONV):
	cd $(TOOLS_DIR)/audioconv64 && make
$(TOOLS_MKSPRITE):
	cd $(TOOLS_DIR)/mksprite && make

# Compile the sort submodule, excluding the basic functions files
$(BUILD_DIR)/algos/%.o: $(ALGO_DIR)/%.c
	@mkdir -p $(dir $@)
	@echo "    [CC] $<"
	$(CC) -c $(CFLAGS) -o $@ $<

$(BUILD_DIR)/structs/%.o: $(DATA_SCTUCTS_DIR)/%.c
	@mkdir -p $(dir $@)
	@echo "    [CC] $<"
	$(CC) -c $(CFLAGS) -o $@ $<

# Convert sprite files
$(SPRITE_FILES_NUMBERS): $(IMAGE_FILES_NUMBERS) $(TOOLS_MKSPRITE)
	@echo "	[MKSPRITE] Processing number image" $(notdir $(subst sprite,png,$@))
	$(TOOLS_MKSPRITE) 16 128 1 $(IMAGES_DIR)/$(notdir $(subst sprite,png,$@)) $@

$(SPRITE_FILES_LETTERS): $(IMAGE_FILES_LETTERS) $(TOOLS_MKSPRITE)
	@echo "	[MKSPRITE] Processing letter image" $(notdir $(subst sprite,png,$@))
	$(TOOLS_MKSPRITE) 32 $(IMAGES_DIR)/$(notdir $(subst sprite,png,$@)) $@

# Convert audio files
$(AUDIO64_FILES): $(AUDIO_FILES) $(TOOLS_AUDIOCONV)
	@echo "	[AUDIOCONV64] " $@
	$(TOOLS_AUDIOCONV) --wav-loop true $(AUDIO_DIR)/$(subst .wav64,.wav,$(notdir $@))
	mv $(notdir $@) $@

include $(N64_INST)/include/n64.mk

$(BUILD_DIR)/$(GAME).dfs: $(SPRITE_FILES_LETTERS) $(SPRITE_FILES_NUMBERS) $(AUDIO64_FILES)
$(BUILD_DIR)/$(GAME).elf: $(OBJS) $(ALGO_OBJECTS) $(DATA_SCTUCTS_OBJECTS)

$(GAME).z64: N64_ROM_TITLE="Sort 64"
$(GAME).z64: $(BUILD_DIR)/$(GAME).dfs

clean:
	rm -rf $(BUILD_DIR)/* *.z64
	rm -rf $(FILESYSTEM_DIR)/*.sprite $(FILESYSTEM_DIR)/*.wav64
	rm $(TOOLS_AUDIOCONV)
	rm $(TOOLS_MKSPRITE)
.PHONY: clean

-include $(wildcard $(BUILD_DIR)/*.d)
