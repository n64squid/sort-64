/* ====================================================================================
	 __  __       _
	|  \/  | __ _(_)_ __    ___
	| |\/| |/ _` | | '_ \  / __|
	| |  | | (_| | | | | || (__
	|_|  |_|\__,_|_|_| |_(_\___|

	This is the entry point to the rest of the program. Its only purpose is to
	initialise the various N64 subsystems and control the flow of all the stages.

	All stage logic is contained within their specific files.

 ==================================================================================== */

#include <libdragon.h>

#include "splash.h"
#include "menu.h"
#include "common.h"
#include "visualiser.h"

void main_loop(void) {
	// Check to see if the start button is pressed so that we can determine the resolution
	controller_scan();
	struct controller_data keys = get_keys_pressed();
	if (keys.c[0].start) {
		resolution = SETTING_RES_LOW;
	}
	// Initialise the display and RDP
	display_init(
		resolution == SETTING_RES_HIGH ? RESOLUTION_640x480 : RESOLUTION_640x240,
		DEPTH_16_BPP,
		2,
		GAMMA_NONE,
		FILTERS_DISABLED
		);
	rdp_init();
	rdp_set_default_clipping();
	dfs_init(DFS_DEFAULT_LOCATION);
	// Initialise the audio subsystem
	audio_init(DEFAULT_FREQUENCY, 3);
	mixer_init(1);
	// Set font colour
	graphics_set_color(
		graphics_make_color(0,0,0,255),
		graphics_make_color(255,255,255,255)
		);

	// Main game loop
	while (1) {
		// Menu
		stage_menu();

		// Visualiser
		stage_vis();
	}
}

int main(void) {
	// Initialise the controllers
	controller_init();

	// Display the splash screen
	splash_screen();

	// Run the main game loop
	main_loop();
}
