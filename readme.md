# Sort 64

Sort 64 is a visualiser for various different soring algorithms that runs on real N64 hardware.

## Features

* 38 algorithms to choose from
* Can have the following lists as inputs
    * Ascending list
    * Descending (reversed) list
    * Repeated items
    * Random (non-linear) inputs
    * Almost-sorted
* Varying array sizes by a factor of 2:
    * 256, 128, 64, 32, 16, 8
* Sound with varying frequency based on the index currently being processed:
    * No sound
    * Sine wave
    * Square wave
    * Triangle wave
    * Sawtooth wave
* Graphical representations:
    * None (bar chart)
    * Trollface
    * 64 Brew
    * Libdragon
* Varying speed:
    * One action per frame
    * Two actions per frame
    * Four actions per frame
    * Eight actions per frame
    * Sixteen actions per frame
    * Thirty-two actions per frame
* Graphical skip at different levels:
    * No skip
    * Skips the array setup
    * Skips data reads
    * Skips swaps
* Different resolutions on startup
    * High-res 640x480i (Default)
    * Low-res 640x240p (Better for CRTs)

## Screenshots

| High res                           | Low res                          |
| ----------------------------------- | ----------------------------------- |
| ![](export/screenshot-1a.png) | ![](export/screenshot-1b.png) |
| ![](export/screenshot-2a.png) | ![](export/screenshot-2b.png) |
| ![](export/screenshot-3a.png) | ![](export/screenshot-3b.png) |
| ![](export/screenshot-4a.png) | ![](export/screenshot-4b.png) |

## More info

Here are some more links about the project:

* [More about Sort 64](https://n64squid.com/projects/sort/sort-64/)
* [More about my general sorting algorithm project](https://n64squid.com/projects/sort/)
* [Repository containing the sorting algorithms](https://gitlab.com/n64squid/sort)
* [Repository containing the data structures used](https://gitlab.com/n64squid/data-structures/)
