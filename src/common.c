/* ====================================================================================
	  ____
	 / ___|___  _ __ ___  _ __ ___   ___  _ __    ___
	| |   / _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \  / __|
	| |__| (_) | | | | | | | | | | | (_) | | | || (__
	 \____\___/|_| |_| |_|_| |_| |_|\___/|_| |_(_\___|

	This just holds a few common functions, constants and variables that are used among
	various files. Most of them are pretty self-explanatory.

 ==================================================================================== */

#include <libdragon.h>

#include "sort/algos/algo_table.h"
#include "common.h"
#include "menu.h"

setting_res_t resolution = SETTING_RES_HIGH;

display_context_t draw_bg(uint8_t detach) {
	// Get a frame buffer
	display_context_t disp;
	while(!(disp = display_lock()));
	// Prepare the RSP
	rdp_enable_primitive_fill();
	rdp_attach(disp);
	rdp_sync(SYNC_PIPE);

	// Draw outer padding
	rdp_set_primitive_color(graphics_make_color(SQUID_RED,SQUID_GREEN,SQUID_BLUE,255));
	rdp_draw_filled_rectangle(
		0,
		0,
		display_get_width(),
		display_get_height()
		);

	// Draw inner rectangle
	rdp_set_primitive_color(graphics_make_color(255,255,255,255));
	rdp_draw_filled_rectangle(
		resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH : SCREEN_PADDING_LOW,
		resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH : SCREEN_PADDING_LOW,
		display_get_width() - (resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH : SCREEN_PADDING_LOW),
		display_get_height() - (resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH : SCREEN_PADDING_LOW)
		);

	if (detach) {
		rdp_detach();
	}
	return disp;
}
