/* ====================================================================================
	__     ___                 _ _
	\ \   / (_)___ _   _  __ _| (_)___  ___ _ __ ___
	 \ \ / /| / __| | | |/ _` | | / __|/ _ | '__/ __|
	  \ V / | \__ | |_| | (_| | | \__ |  __| |_| (__
	   \_/  |_|___/\__,_|\__,_|_|_|___/\___|_(_)\___|

	This is the most important part of the ROM itself. It is where the visualisation of
	the sorting algorithm actually occurs.

	stage_vis() runs only once - it's the function that sets up the stage and loads all
	necessary things into memory.

	display_vis() is the function that is called every time a sorting algorithm calls a
	comparison, swap, insert or read function. It pretty much runs every frame and paints
	the frame buffer and fills the audio buffer (if needed).

 ==================================================================================== */

#include <libdragon.h>

#include "sort/algos/algo_table.h"
#include "sort/algos/exchange_sort.h"
#include "sort/algos/insertion_sort.h"
#include "sort/algos/random_sort.h"
#include "sort/algos/basic_funcs.h"
#include "sort/data-structures/array.h"
#include "common.h"
#include "menu.h"
#include "visualiser.h"

#define ARRAY_PADDING 50
#define ARRAY_WIDTH 512
#define DESCRIPTION_X_PADDING 200
#define DESCRIPTION_Y_PADDING_HIGH 360
#define DESCRIPTION_Y_PADDING_LOW 180
#define DESCRIPTION_WIDTH 45

// Global variables that hold the array
int* vis_array;
int vis_array_len;
// Counter for skipping frames to make it go faster
uint8_t counter = 0;
// Var to tell whether we should skip visualisation for now
uint8_t skip_vis = 0;
// Sprite to use
sprite_t* sprite_1 = NULL;
sprite_t* sprite_2 = NULL;
// Audio to use
wav64_t wav;
wav64_t wavs[4] = {NULL};
// Skip everything
uint8_t stop_vis = 0;

// Wonky prototype
void reset_metrics(void);

// Print a farmatted string showing the description of the algorithm chosen
void print_description(display_context_t disp, algo_row* row) {
	char text[ALGOTABLE_DESC_LEN];	// Contains the concatenated text
	char* p = text;					// Pointer to the current position in text
	uint8_t i = 0;					// Pointer to the current position in the line

	// Concatenate the strings
	sprintf(text, "%s: %s", row->name, row->desc);

	// Scan through the whole string
	while (*p) {
		// Reached width limit, create a new line
		if (i == DESCRIPTION_WIDTH) {
			// Move left until we reach a space
			while (*p != ' ') {
				p--;
			}
			// Replace with a newline
			*p = '\n';
			i = 0;
		}
		i++;
		p++;
	}
	// Draw the string to the screen
	graphics_draw_text(
		disp,
		(resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH+MENU_PADDING_HIGH : SCREEN_PADDING_LOW+MENU_PADDING_LOW)+DESCRIPTION_X_PADDING,
		(resolution == SETTING_RES_HIGH ? DESCRIPTION_Y_PADDING_HIGH : DESCRIPTION_Y_PADDING_LOW),
		text
		);
}

void display_vis(int* pointer_a, int* pointer_b, uint32_t colour) {
	// Perform a speed check
	if (stop_vis || skip_vis || (counter++) % (1 << settings[SETTING_SPEED])) {
		return;
	}
	display_context_t disp;
	// Check to see whether we're drawing a bar chart or an image
	if (!settings[SETTING_IMAGE]) {
		// Draw the array using bars
		disp = draw_bg(DRAW_BG_NO_DETACH);
		for (int i=0; i<vis_array_len; i++) {
			if (i == pointer_a - vis_array || i == pointer_b - vis_array ) {
				rdp_set_primitive_color(colour);
			} else {
				rdp_set_primitive_color(SQUID_COLOUR_16);
			}
			if (resolution == SETTING_RES_HIGH) {
				rdp_draw_filled_rectangle(
					SCREEN_WIDTH/2-ARRAY_WIDTH/2+(i<<settings[SETTING_ARRAY_SIZE])*2,
					SCREEN_PADDING_HIGH+ARRAY_PADDING+(ARRAY_WIDTH/2)-(vis_array[i]<<settings[SETTING_ARRAY_SIZE]),
					SCREEN_WIDTH/2-ARRAY_WIDTH/2+((1<<settings[SETTING_ARRAY_SIZE])+(i<<settings[SETTING_ARRAY_SIZE]))*2-1,
					SCREEN_PADDING_HIGH+ARRAY_PADDING+ARRAY_WIDTH/2
					);
			} else {
				rdp_draw_filled_rectangle(
					SCREEN_WIDTH/2-ARRAY_WIDTH/2+(i<<settings[SETTING_ARRAY_SIZE])*2,
					SCREEN_PADDING_HIGH+ARRAY_PADDING+(ARRAY_WIDTH/5)-(vis_array[i]<<(settings[SETTING_ARRAY_SIZE]))/2,
					SCREEN_WIDTH/2-ARRAY_WIDTH/2+((1<<settings[SETTING_ARRAY_SIZE])+(i<<settings[SETTING_ARRAY_SIZE]))*2-1,
					(SCREEN_PADDING_HIGH+ARRAY_PADDING+ARRAY_WIDTH/5)
					);
			}
		}
		rdp_detach();
	} else {
		// Draw the array using a sprite image
		disp = draw_bg(DRAW_BG_DETACH);
		// Loop through all elements in the array
		for (int i=0; i<vis_array_len; i++) {
			if (i == pointer_a - vis_array || i == pointer_b - vis_array ) {
				// Draw the coloured rectangle
				graphics_draw_box(
					disp,
					SCREEN_WIDTH/2-ARRAY_WIDTH/2+((i<<settings[SETTING_ARRAY_SIZE]))*2,
					(resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH+ARRAY_PADDING : SCREEN_PADDING_LOW+ARRAY_PADDING/2),
					2 << settings[SETTING_ARRAY_SIZE],
					ARRAY_WIDTH/(resolution == SETTING_RES_HIGH ? 2 : 4),
					colour
					);
			} else {
				// Loop through the slices to draw (for when we have fewer than the max elements in the array)
				for (int j=0; j<1<<settings[SETTING_ARRAY_SIZE]; j++) {
					// Draw slice of the image
					graphics_draw_sprite_trans_stride(
						disp,
						SCREEN_WIDTH/2-ARRAY_WIDTH/2+((i<<settings[SETTING_ARRAY_SIZE])+j)*2,
						(resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH+ARRAY_PADDING : SCREEN_PADDING_LOW+ARRAY_PADDING/2),
						vis_array[i] < vis_array_len/2 ? sprite_1 : sprite_2,
						((vis_array[i]<<settings[SETTING_ARRAY_SIZE])+j) % (vis_array_len/2<<settings[SETTING_ARRAY_SIZE])
						);
				}
			}
			
		}
	}
	char text[128];
	// Draw the title
	sprintf(
		text, 
		"You are now viewing: %s at speed: %i. Use <-C-> to adjust.", 
		list_of_algos.rows[settings[SETTING_ALGORITHM]].name,
		settings[SETTING_SPEED]
		);
	if (resolution == SETTING_RES_HIGH) {
		graphics_draw_text(
			disp, SCREEN_PADDING_HIGH+MENU_PADDING_HIGH+X_PADDING, SCREEN_PADDING_HIGH+MENU_PADDING_HIGH, text
			);
	} else {
		graphics_draw_text(
			disp, SCREEN_PADDING_LOW+MENU_PADDING_LOW+X_PADDING, SCREEN_PADDING_LOW+MENU_PADDING_LOW, text
			);
	}

	// Let's draw some stats
	for (int i=0, j=0; i<BASIC_METRICS && (resolution == SETTING_RES_HIGH || j<2); i++) {
		if (
			list_of_algos.rows[settings[SETTING_ALGORITHM]].estimation_func == NULL || 
			(*list_of_algos.rows[settings[SETTING_ALGORITHM]].estimation_func)(vis_array_len, i) == 0
		) {
			continue;
		}
		sprintf(
			text, 
			"%.4s test: %lli\n%.4s estm: %lli",
			metrics[i].name, 
			get_metric(i),
			metrics[i].name, 
			list_of_algos.rows[settings[SETTING_ALGORITHM]].estimation_func ? (*list_of_algos.rows[settings[SETTING_ALGORITHM]].estimation_func)(vis_array_len, i) : 0
			);
		graphics_draw_text(
			disp,
			(resolution == SETTING_RES_HIGH ? SCREEN_PADDING_HIGH+MENU_PADDING_HIGH+X_PADDING : SCREEN_PADDING_LOW+MENU_PADDING_LOW+X_PADDING),
			(resolution == SETTING_RES_HIGH ? DESCRIPTION_Y_PADDING_HIGH : DESCRIPTION_Y_PADDING_LOW)+(j++*20),
						   text
			);

	}
	
	print_description(disp, &list_of_algos.rows[settings[SETTING_ALGORITHM]]);

	// Push the video output to the screen
	display_show(disp);

	// Handle the audio
	if (settings[SETTING_AUDIO] && pointer_a >= vis_array && audio_can_write()) {
		pointer_a = pointer_a < (vis_array + vis_array_len) ? pointer_a : vis_array + vis_array_len/2;
		pointer_b = pointer_b < (vis_array + vis_array_len) ? pointer_b : vis_array + vis_array_len/2;
		short *buf = audio_write_begin();
		mixer_poll(buf, audio_get_buffer_length());
		mixer_ch_set_freq(
			0, 
			DEFAULT_FREQUENCY/2 + 
			DEFAULT_FREQUENCY/2 * ((float)abs(pointer_a - vis_array)/vis_array_len) +
			(pointer_b ? DEFAULT_FREQUENCY/2 * ((float)abs(pointer_b - vis_array)/vis_array_len) : 0)
			);
		audio_write_end();
	}

	// Check for controller input
	controller_scan();
	struct controller_data keys = get_keys_down();
	if (keys.c[0].data) {
		if (keys.c[0].C_left) {
			// Decrease speed
			settings[SETTING_SPEED] = settings[SETTING_SPEED] > 0 ? settings[SETTING_SPEED]-1 : settings[SETTING_SPEED];
		} else if (keys.c[0].C_right) {
			// Increase speed
			settings[SETTING_SPEED] = settings[SETTING_SPEED] < 10 ? settings[SETTING_SPEED]+1 : settings[SETTING_SPEED];
		} else if (keys.c[0].start) {
			// Skip fucking everything
			stop_vis = 1;
		}
	}
}

void stage_vis(void) {

	// Adjust the array size
	while (ARRAY_SIZE_MAX >> (settings[SETTING_ARRAY_SIZE]+1) > list_of_algos.rows[settings[SETTING_ALGORITHM]].max) {
		settings[SETTING_ARRAY_SIZE]++;
	}

	// Allocate memory for the array
	vis_array = calloc(ARRAY_SIZE_MAX >> settings[SETTING_ARRAY_SIZE], sizeof(int));
	vis_array_len = ARRAY_SIZE_MAX >> settings[SETTING_ARRAY_SIZE];
	// Check whether to skip this part
	skip_vis = settings[SETTING_SKIP] > SETTING_SKIP_NONE ? 1 : 0;

	char filename[64] = "";
	// Load sprite if needed
	if (settings[SETTING_IMAGE]) {
		sprintf(filename, "/%ia%i.sprite", settings[SETTING_IMAGE], resolution);
		int fp = dfs_open(filename);
		sprite_1 = malloc(dfs_size(fp));
		dfs_read(sprite_1, 1, dfs_size(fp), fp);
		dfs_close(fp);

		sprintf(filename, "/%ib%i.sprite", settings[SETTING_IMAGE], resolution);
		fp = dfs_open(filename);
		sprite_2 = malloc(dfs_size(fp));
		dfs_read(sprite_2, 1, dfs_size(fp), fp);
		dfs_close(fp);
	}

	// Load audio if not loaded already
	if (settings[SETTING_AUDIO]) {
		// Play WAV files
		sprintf(filename, "rom:/%i.wav64", settings[SETTING_AUDIO]);
		wav64_open(&wavs[settings[SETTING_AUDIO]-1], filename);
		wav64_set_loop(&wavs[settings[SETTING_AUDIO]-1], true);
		wav64_play(&wavs[settings[SETTING_AUDIO]-1], 0);
	}

	// Generate the array values
	srand(TICKS_READ());
	switch (settings[SETTING_ARRAY_TYPE]) {
		case SETTING_ARRAY_TYPE_RANDOM:
			generate_random_array(vis_array, vis_array_len);
			break;
		case SETTING_ARRAY_TYPE_DESC:
			generate_reverse_array(vis_array, vis_array_len);
			break;
		case SETTING_ARRAY_TYPE_REPEATING:
			generate_random_repeating_array(vis_array, vis_array_len);
			break;
		case SETTING_ARRAY_TYPE_DOUBLE_RANDOM:
			generate_random_random_array(vis_array, vis_array_len);
			break;
		case SETTING_ARRAY_TYPE_ALMOST:
			generate_almost_sorted_array(vis_array, vis_array_len);
			break;
		default:
			generate_random_array(vis_array, vis_array_len);
			break;
	}
	// Reactivate the skipper
	skip_vis = 0;
	stop_vis = 0;
	reset_metrics();
	
	// Run the sorting algorithm
	(*list_of_algos.rows[settings[SETTING_ALGORITHM]].func)(vis_array, vis_array_len);

	// Draw a final clear display
	counter = 0;
	display_vis(NULL, NULL, 0);

	// Wait for controller input to complete the stage
	struct controller_data keys;
	while (1) {
		controller_scan();
		keys = get_keys_down();
		if (keys.c[0].data) {
			if (keys.c[0].A) {
				// Free the memory
				free(sprite_1);
				free(sprite_2);
				free(vis_array);

				// Stop the audio on channel zero
				if (settings[SETTING_AUDIO]) {
					mixer_ch_stop(0);
				}
				break;
			}
		} else if (stop_vis) {
			// Ignore this if we're skipping
			break;
		}
	}
}
